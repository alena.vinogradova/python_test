import csv
import io
import re


class Parser:
    def parser_fsck(self, file_path):
        with open(file_path, 'r', encoding='utf-8') as file:
            lines = file.readlines()

        result = []

        # Регулярное выражение для проверки, что это файл с расширением ".parq"
        parquet_pattern = re.compile(r'.*\/([^\/]+\.parq)\s+\d+ bytes')

        # Регулярное выражение для проверки, что файл находится не менее, чем
        # на 5 уровне иерархии
        hierarchy_level_pattern = re.compile(r'^\/(?:[^\/]+\/){4,}.*$')

        # Регулярное выражение для извлечения имени таблицы (4-й уровень
        # иерархии)
        table_name_pattern = re.compile(r'\/(?:[^\/]+\/){3}([^\/]+)\/')

        # Регулярное выражение для извлечения кол-ва блоков и целостности блока
        block_info1_pattern = re.compile(r'.*(\d+) block\(s\):  \s*(\w+)')

        # Регулярное выражение для извлечения подробной информации о блоке
        block_info2_pattern = re.compile(r'(.*) len=(\d+) Live_repl=(\d+)')

        i = 0
        while i < len(lines):
            line = lines[i]

            parquet_match = parquet_pattern.match(line)
            hierarchy_level_match = hierarchy_level_pattern.match(line)

            # Проверяем два условвия: уровень файла и расширение
            if parquet_match and hierarchy_level_match:
                # Извлекаем имя файла
                filename = parquet_match.group(1)
                table_name = table_name_pattern.match(line).group(1)
                block_info1_match = block_info1_pattern.match(line)
                blocks_count = int(block_info1_match.group(1))
                blocks_lens = []
                i += 1
                for j in range(blocks_count):
                    block_info2_match = block_info2_pattern.match(lines[i + j])
                    block_length = block_info2_match.group(2)
                    blocks_lens.append(int(block_length))
                i += j
                # Определяем средний размер блока, для каждого паркет файла
                avg_blocks_lens = sum(blocks_lens)/len(blocks_lens) if len(blocks_lens) != 0 else None

                # Запись результата в словарь
                result.append({
                    "Table": table_name,
                    "FileName": filename,
                    "Blocks": blocks_count,
                    "AvgBlockLength": avg_blocks_lens,
                })
            i += 1

        return result

    def get_csv_string(self, data):
        # Поля для CSV
        fields = [
            "Table",
            "FileName",
            "Blocks",
            "AvgBlockLength"]

        # Используем StringIO для хранения CSV строки в памяти
        csv_output = io.StringIO()

        # Записываем данные в CSV строку
        writer = csv.DictWriter(csv_output, fieldnames=fields)
        writer.writeheader()
        writer.writerows(data)

        # Получаем строку из StringIO
        csv_string = csv_output.getvalue()

        # Закрываем StringIO
        csv_output.close()

        return csv_string

    def Parse(self, file_path):
        return self.get_csv_string(self.parser_fsck(file_path))


# Пример использования
if __name__ == "__main__":
    parser = Parser()
    result = parser.Parse("test.txt")
    print(result)
