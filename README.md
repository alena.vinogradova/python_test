В файле *txt_to_csv.py* описан класс **Parser**, метод **Parse** преобразует результат выполнения  команды *hdfs fsck  / -files -blocks* в строку структуры csv с полями:

*Table, FileName, Blocks, AvgBlockLength.*

--------------------------

Пример входных данных в файле test.txt.

Пример вывода метода Parse класса Parser, преобразованный в таблицу для наглядности.

| Table                      | FileName                                                 | Blocks | AvgBlockLength    |
| -------------------------- | -------------------------------------------------------- | ------ | ----------------- |
| al_subscriber_x_action_tab | 984a3966b085a9cc-42fd23cc0000002c_1835864861_data.0.parq | 7      | 9.142857142857142 |
| al_subscriber_x_action_tab | 984a3966b085a9cc-42fd23cc0000002d_821310527_data.0.parq  | 2      | 21.5              |
| al_subscriber_x_action_tab | 984a3966b085a9cc-42fd23cc0000002e_972365053_data.0.parq  | 1      | 20.0              |
| al_subscriber_x_action_tab | bf417aeac4ab0587-e4b5a9b40000002b_530865038_data.0.parq  | 0      |                   |
|                            |                                                          |        |                   |
